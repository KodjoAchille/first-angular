import { Component, Input, OnInit } from '@angular/core';
import { AppareilService } from '../services/appareil.service';

@Component({
  selector: 'app-appareil',
  templateUrl: './appareil.component.html',
  styleUrls: ['./appareil.component.scss']
})
export class AppareilComponent implements OnInit {
  @Input() name: string;
  @Input() status: string;
  @Input() index: number =0;
  constructor( private appareilService: AppareilService) { this.name="vide" ; this.status="éteint"}
  ngOnInit(): void {
  }
  getStatus(){
    return this.status;
  }
  getColor(){
    return this.status=="éteint"?"red":"green";
  }
  onAllumer( i: number){
    this.appareilService.swithchOnOne(i);
  }

  onEteindre( i: number){
    if(confirm("Etes-vous sûr de vouloir supprimer l'appareil "+(i+1) +"?"))
    this.appareilService.swithchOffOne(i);
  }
}
