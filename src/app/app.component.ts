import { Component, OnInit } from '@angular/core';
import { AppareilService } from './services/appareil.service';
import { interval } from 'rxjs';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  implements OnInit{
  constructor( private appareilService: AppareilService) {
  }

  secondes:number=0;
  counterSubscription! : Subscription ;

  ngOnInit(){
    const counter = interval(1000);
    this.counterSubscription=counter.subscribe(
      (value) =>{
        this.secondes =value;
      },
      (error) =>{
        console.log('Uh-oh, an error occured ! :' + error);
      },
      ()=>{
        console.log('Observable comple!')
      }
    );
  }

  ngOnDestroy(){
    this.counterSubscription.unsubscribe();
  }
  
}
