import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppareilService } from '../services/appareil.service';

@Component({
  selector: 'app-single-appareil',
  templateUrl: './single-appareil.component.html',
  styleUrls: ['./single-appareil.component.scss']
})
export class SingleAppareilComponent implements OnInit {

  constructor(private appareilService: AppareilService, private route: ActivatedRoute) { 

  }

  appareil={
     status:"",
     name:"",
  };

  name: string= 'Appareil';
  status: string= 'statut';

  ngOnInit(): void {
    //this.getOneInfo(this.route.snapshot.params['id'])
    const id = this.route.snapshot.params['id'];
    
    //this.status = this.appareilService.getAppareilById(+id).status;
  }

  getOneInfo(id: number)
  {
     //this.appareil=this.appareilService.getOneInfo(id);
  }


}
